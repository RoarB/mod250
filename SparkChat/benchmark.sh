#!/bin/bash

# Usage: time ./benchmark.sh <baseUrl> <numIterations>
# E.g: time ./benchmark.sh https://localhost:8181/EEChat 500
#      time ./benchmark.sh https://localhost:4567 500

baseUrl=$1
numIterations=$2

for i in `seq 1 $numIterations`;
do
    # The user roar gets the messages, then adds a new message
    curl --user roar:roar -k $baseUrl/api/channels/Bergen/messages 2>/dev/null 1>&2
    curl --user roar:roar -k -H "Content-Type: application/json" -X POST -d '{author:"roar",text:"Whats up?"}' $baseUrl/api/channels/Bergen/messages 2>/dev/null 1>&2

    # The user sanaullah gets the messages, then adds a new message
    curl --user sanaullah:sanaullah -k $baseUrl/api/channels/Bergen/messages 2>/dev/null 1>&2
    curl --user sanaullah:sanaullah -k -H "Content-Type: application/json" -X POST -d '{author:"sanaullah",text:"Not much"}' $baseUrl/api/channels/Bergen/messages 2>/dev/null 1>&2
    echo -n "."
done

