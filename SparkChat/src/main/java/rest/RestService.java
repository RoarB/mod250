package rest;

import com.google.gson.*;
import java.lang.reflect.Type;
import static java.net.HttpURLConnection.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.Map.Entry;
import model.Channel;
import model.Message;
import model.User;
import spark.Response;
import spark.Request;
import static spark.Spark.*;

/**
 * The class of the REST service implemented using Spark/Java
 * @author Roar, Sanaullah
 */
public class RestService {
    private final Map<String, Channel> channels = new HashMap<>();
    private final Map<String, User> users = new HashMap<>();
    private final List<ChannelUpdatedListener> channelUpdatedListeners = new ArrayList<>();
    private final Gson gson;

    /**
     * Construct a new REST service
     */
    public RestService() {
        gson = new GsonBuilder().registerTypeAdapter(Message.class, new MessageDeserializer()).create();
        
        // Put some default data into the model
        channels.put("Bergen", new Channel("Bergen"));
        channels.put("MOD250", new Channel("MOD250"));
        channels.put("Spark", new Channel("Spark"));
        users.put("roar", new User("roar", "roar", "Roar Bergheim"));
        users.put("sanaullah", new User("sanaullah", "sanaullah", "Sanaullah Afridi"));
    }
    
    /**
     * Add a listener to the channel updated event
     * @param listener The new listener
     */
    public void addChannelUpdatedListener(ChannelUpdatedListener listener) {
        channelUpdatedListeners.add(listener);
    }
    
    /**
     * Start the REST service
     */
    public void startService() {
        setupRoutes();
    }
    
    private void setupRoutes() {
        // Set up the routes for getting the list of channels, and adding a new
        get("/api/channels", (req, res) -> respondJson(res, channels.values()));
        post("/api/channels", (req, res) -> addChannel(req, res));

        // Set up the routes for getting the details of a user, and adding a new user
        get("/api/users/:username", (req, res) -> {
            User reqUser = users.get(req.params("username"));
            User authUser = getAuthenticatedUser(req);
            if (reqUser != authUser)
                halt(HTTP_FORBIDDEN);
            return respondJson(res, authUser);
        });
        post("/api/users", (req, res) -> addUser(req, res));
        
        // Set up the routes for getting the list of messages in a channel, 
        // adding a new message and liking a message
        get("/api/channels/:channelName/messages", (req, res) -> {
            ensureAuthenticatedUser(req);
            Channel chan = channels.get(req.params("channelName"));
            return respondJson(res, chan.getMessages());
        });
        post("/api/channels/:channelName/messages", (req, res) -> {
            ensureAuthenticatedUser(req);
            Channel chan = channels.get(req.params("channelName"));
            try {
                Message newMessage = gson.fromJson(req.body(), Message.class);
                chan.getMessages().put(newMessage.getId(), newMessage);
                notifyChannelUpdated(chan.getChannelName());
                return "";
            }
            catch (JsonSyntaxException e) {
                res.status(HTTP_BAD_REQUEST);
                return e.getMessage();
            }
        });
        post("/api/channels/:channelName/messages/:msgId/likedBy", (req, res) -> {
            User user = ensureAuthenticatedUser(req);
            Channel chan = channels.get(req.params("channelName"));
            try {
                Long msgId = Long.valueOf(req.params("msgId"));
                Message msg = chan.getMessages().get(msgId);
                msg.getLikedBy().add(user);
                notifyChannelUpdated(chan.getChannelName());
                return "";
            }
            catch (JsonSyntaxException e) {
                res.status(HTTP_BAD_REQUEST);
                return e.getMessage();
            }
        });
    }

    private User ensureAuthenticatedUser(Request req) {
        User user = getAuthenticatedUser(req);
        if (user == null)
            halt(HTTP_FORBIDDEN);
        return user;
    }
    
    private User getAuthenticatedUser(Request req) {
        // We expect a header on the form "Authorization: Basic af8u2jl="
        String authHeader = req.headers("Authorization");
        if (authHeader == null)
            return null;
        String[] authSplit = authHeader.split(" ");
        if (!"Basic".equals(authSplit[0]))
            return null;
        
        // The last part is username:password Base64-encoded
        String decodedAuth = new String(Base64.getDecoder().decode(authSplit[1]), StandardCharsets.UTF_8);
        String[] decodedSplit = decodedAuth.split(":");
        User user = users.get(decodedSplit[0]);
        if (user == null)
            return null;
        if (user.getPassword() == null ? decodedSplit[1] != null : !user.getPassword().equals(decodedSplit[1]))
            return null;
        
        // The user has been found and the password match
        return user;
    }
    
    private String addChannel(Request req, Response res) {
        try {
            Channel newChannel = gson.fromJson(req.body(), Channel.class);
            channels.put(newChannel.getChannelName(), newChannel);
            return "";
        } 
        catch (JsonSyntaxException e) {
            res.status(HTTP_BAD_REQUEST);
            return e.getMessage();
        }
    }
    
    private String respondJson(Response res, Object obj) {
        res.status(200);
        res.type("application/json");
        return gson.toJson(obj);
    }

    private String addUser(Request req, Response res) {
        try {
            User newUser = gson.fromJson(req.body(), User.class);
            users.put(newUser.getUsername(), newUser);
            return "";
        } 
        catch (JsonSyntaxException e) {
            res.status(HTTP_BAD_REQUEST);
            return e.getMessage();
        }
    }
    
    private void notifyChannelUpdated(String channelName) {
        channelUpdatedListeners.forEach((listener) -> {
            listener.channelUpdated(channelName);
        });
    }
    
    /**
     * Custom deserializer for JSON messages. The reason for it is to look up
     * the user. The JSON just contains a username, but we need a User object.
     */
    private class MessageDeserializer implements JsonDeserializer<Message> {
        @Override
        public Message deserialize(JsonElement json, Type type, JsonDeserializationContext jdc) throws JsonParseException {
            Message msg = new Message();
            Set<Entry<String, JsonElement>> messageProperties = json.getAsJsonObject().entrySet();
            for (Entry<String, JsonElement> entry : messageProperties) {
                switch (entry.getKey()) {
                    case "text":
                        msg.setText(entry.getValue().getAsString());
                        break;
                    case "author":
                        User user = users.get(entry.getValue().getAsString());
                        msg.setAuthor(user);
                }
            }
            return msg;
        }
    }
}
