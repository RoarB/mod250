package notify;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import rest.ChannelUpdatedListener;
import static spark.Spark.webSocket;

/**
 * A class for notifying clients of updates of channels using web sockets
 * @author Roar, Sanaullah
 */
@WebSocket
public class NotifySocket implements ChannelUpdatedListener {
    private final Queue<SubscribingSession> sessions = new ConcurrentLinkedQueue<>();

    /**
     * Sets up the web socket at the /notify endpoint
     */
    public void setupWebSocket() {
        webSocket("/notify", this);
    }

    /**
     * Event handler for the socket connected event
     * @param session The web socket session object
     */
    @OnWebSocketConnect
    public void connected(Session session) {
        sessions.add(new SubscribingSession(session));
    }

    /**
     * Event handler for the socket closed event
     * @param session The web socket session object
     * @param statusCode The status code (ignored)
     * @param reason The close reason (ignored)
     */
    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) {
        sessions.remove(findSubscribingSession(session));
    }

    /**
     * Event handler for new messages received through the web socket
     * @param session The web socket session object
     * @param message The received message
     */
    @OnWebSocketMessage
    public void message(Session session, String message) {
        // We expect a message of the form "subscribe:ChannelName"
        String[] messageParts = message.split(":");
        if (!"subscribe".equals(messageParts[0]))
            return;
        
        // Find the session and set the channel subscription on it
        findSubscribingSession(session).setChannelSubscription(messageParts[1]);
    }

    /**
     * Implements the channelUpdated event. Sends a message with just the 
     * channel name to all messages that are subscribing to this channel
     * @param channelName The name of the updated channel
     */
    @Override
    public void channelUpdated(String channelName) {
        // Iterate through all the sessions
        for (SubscribingSession subSession: sessions) {
            // If this session is subscribing to this channel, send a message to it
            if (channelName.equals(subSession.getChannelSubscription())) {
                try {
                    subSession.getSession().getRemote().sendString(channelName);
                } catch (IOException ex) {
                    Logger.getLogger(NotifySocket.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private SubscribingSession findSubscribingSession(Session session) {
        for (SubscribingSession subSession: sessions) {
            if (subSession.getSession().equals(session)) {
                return subSession;
            }
        }
        return null;
    }
}
