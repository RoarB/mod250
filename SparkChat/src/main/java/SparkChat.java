import notify.NotifySocket;
import rest.RestService;
import static spark.Spark.secure;
import static spark.Spark.staticFiles;

/**
 * Main program for the SparkChat chat room application
 * @author Roar, Sanaullah
 */
public class SparkChat {

    /**
     * Start the main program
     * @param args Command line arguments (none)
     */
    public static void main(String[] args) {
        // Enable HTTPS/SSL
        secure("keystore.jks", "password", null, null);
        
        // Set up the web socket used for notification of new messages
        NotifySocket notifySocket = new NotifySocket();
        notifySocket.setupWebSocket();
        
        // Specify location of the HTML files
        staticFiles.externalLocation("../ChatFrontend/public_html");
        
        // Set up the REST service and connect it with the notification socket
        RestService service = new RestService();
        service.addChannelUpdatedListener(notifySocket);
        service.startService();
    }
}
