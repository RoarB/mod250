/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jms;

import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.MessageListener;
import javax.jms.Topic;

/**
 *
 * @author roar
 */
public class BidMeTopicConsumer {
    @Resource(lookup = "jms/AuctionEndedFactory")
    private ConnectionFactory connectionFactory;
    @Resource(lookup = "jms/AuctionEnded")
    private Topic topic;
    JMSContext context;

    public BidMeTopicConsumer(MessageListener listener) {
        context = connectionFactory.createContext();
        JMSConsumer consumer = context.createConsumer(topic);
        consumer.setMessageListener(listener);
    }
}
