/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bidmeclient;

import javax.xml.ws.WebServiceRef;
import java.util.List;
import soap.Bid;
import soap.BidMeSOAP_Service;
import soap.BidMeUser;
import soap.BidResponseMessage;
import soap.Product;
import view.MainFrame;

/**
 *
 * @author roar
 */
public class Main {

    @WebServiceRef(wsdlLocation = "META-INF/wsdl/localhost_8080/BidMe/BidMeSOAP.wsdl")
    private static BidMeSOAP_Service service;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //MainFrame mainFrame = new MainFrame();
        //mainFrame.setVisible(true);
        MainFrame.main(args);
    }

    public static List<Product> getActiveAuctions() {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        soap.BidMeSOAP port = service.getBidMeSOAPPort();
        return port.getActiveAuctions();
    }

    public static Bid getCurrentBid(Long productId) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        soap.BidMeSOAP port = service.getBidMeSOAPPort();
        return port.getCurrentBid(productId);
    }

    public static BidMeUser getUser(String username) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        soap.BidMeSOAP port = service.getBidMeSOAPPort();
        return port.getUser(username);
    }

    public static BidResponseMessage bidForAuction(Long productId, String username, Double amount) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        soap.BidMeSOAP port = service.getBidMeSOAPPort();
        return port.bidForAuction(productId, username, amount);
    }
}
