/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bidmeresttester;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 *
 * @author roar
 */
public class BidMeRestTester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        getRestJson("http://localhost:8080/BidMe/api/product/activeAuctions");
        getRestJson("http://localhost:8080/BidMe/api/product/10003");
    }

    private static void getRestJson(String url) throws RuntimeException {
        Client client = ClientBuilder.newClient();
        
        WebTarget target = client.target(url);
        Response response = target.request().accept("application/json").get();
        if (response.getStatus() != 200)
            throw new RuntimeException("HTTP error : " + response.getStatus());
        
        String output = response.readEntity(String.class);
        System.out.println("Getting URL: " + url);
        System.out.println(output);
    }
}
