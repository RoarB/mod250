/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import boundary.ProductFacade;
import boundary.CategoryFacade;
import entities.Bid;
import entities.BidMeUser;
import entities.Category;
import entities.Product;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

/**
 *
 * @author Roar, Sanaullah
 */
@Named(value = "productDetailsBean")
@RequestScoped
public class ProductDetailsBean {
    private Long productId;
    private Product product;

    private String name;
    private String description;
    private LocalDateTime auctionStartDate;
    private LocalDateTime auctionEndDate;
    private Double startPrice;
    private String seller;
    private Long categoryId;
    
    @Inject
    private LoginBean loginBean;
    
    @EJB
    private CategoryFacade categoryFacade;
    
    @EJB
    private ProductFacade productFacade;

    @Resource(lookup = "jms/NotifyBuyerFactory")
    private ConnectionFactory connectionFactory;
    @Resource(lookup = "jms/NotifyBuyer")
    private Queue notifyBuyerQueue;
    @Resource(lookup = "jms/AuctionEndedFactory")
    private ConnectionFactory topicConnectionFactory;
    @Resource(lookup = "jms/AuctionEnded")
    private Topic auctionEndedTopic;

    /**
     * Creates a new instance of ProductDetailsBean
     */
    public ProductDetailsBean() {
    }

    public Double getCurrentBid() {
        if (productId == null)
            return 0.0;
        Bid highestBid = productFacade.getCurrentBid(productId);
        if (highestBid == null)
            return 0.0;
        return highestBid.getAmount();
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuctionStartDate() {
        return dateToString(auctionStartDate);
    }

    public void setAuctionStartDate(String auctionStartDate) {
        this.auctionStartDate = stringToDate(auctionStartDate);
    }

    public String getAuctionEndDate() {
        return dateToString(auctionEndDate);
    }

    public void setAuctionEndDate(String auctionEndDate) {
        this.auctionEndDate = stringToDate(auctionEndDate);
    }

    public Double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Double startPrice) {
        this.startPrice = startPrice;
    }
    
    public void setProductId(Long productId) {
        this.productId = productId;
    }    

    public Long getProductId() {
        return productId;
    }
    
    public Product getProduct() {
        return product;
    }

    public void init() {
        if (productId != null) {
            product = productFacade.find(productId);
            updateProperties();
        }
    }

    public String postProduct() {
        boolean isNewProduct = productId == null;
        if (isNewProduct)
            product = new Product();
        else
            product = productFacade.find(productId);
        updateProductEntity();
        if (isNewProduct)
            productFacade.create(product);
        else
            productFacade.edit(product);
        return "viewProduct?faces-redirect=true&productId=" + product.getId().toString();
    }
    
    public String triggerAuctionEnd(Long productId) throws JMSException {
        Product triggerProduct = productFacade.find(productId);
        Bid highestBid = productFacade.getCurrentBid(productId);
        if (highestBid == null) {
            Logger.getLogger(ProductDetailsBean.class.getName()).log(Level.INFO, "Auction ended without any bidders\n");
            return "viewProduct?faces-redirect=true&productId=" + productId.toString();
        }
        
        BidMeUser winner = highestBid.getBidder();
        
        Connection connection = connectionFactory.createConnection(); 
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer queueProducer = session.createProducer(notifyBuyerQueue);
        TextMessage message = session.createTextMessage();
        message.setText("--- START EMAIL to customer " + winner.getUsername() + " ---\n"
            + "Dear " + winner.getFirstName() + "\n"
            + "Congratulations, you won the bid for product " + triggerProduct.getName() + "\n"
            + "Product URL: https://localhost:8181/faces/viewProduct?faces-redirect=true&productId=" + productId.toString() + "\n"
            + "--- END EMAIL ---"); 
        queueProducer.send(message);
        
        MessageProducer topicProducer = session.createProducer(auctionEndedTopic);
        TextMessage topicMessage = session.createTextMessage();
        topicMessage.setText("Auction for product id=" + triggerProduct.getName() + "ended"); 
        topicProducer.send(topicMessage);
                
        return "viewProduct?faces-redirect=true&productId=" + productId.toString();
    }
    
    private void updateProperties() {
        name = product.getName();
        description = product.getDescription();
        auctionStartDate = product.getAuctionStartDate();
        auctionEndDate = product.getAuctionEndDate();
        startPrice = product.getStartPrice();
        seller = product.getSeller().getUsername();
        categoryId = product.getCategory().getId();
    }
    
    private void updateProductEntity() {
        if (name != null)
            product.setName(name);
        if (description != null)
            product.setDescription(description);
        if (auctionStartDate != null)
            product.setAuctionStartDate(auctionStartDate);
        if (auctionEndDate != null)
            product.setAuctionEndDate(auctionEndDate);
        if (startPrice != null)
            product.setStartPrice(startPrice);
        BidMeUser sellerUser = loginBean.getLoggedInUser();
        if (sellerUser != null)
            product.setSeller(sellerUser);
        if (categoryId != null) {
            Category category = categoryFacade.find(categoryId);
            if (category != null)
                product.setCategory(category);
        }
    }

    private String dateToString(LocalDateTime date) {
        if (date == null)
            return "";
        return date.toString();
    }

    private static LocalDateTime stringToDate(String date) {
        try {
            return LocalDateTime.parse(date);
        }
        catch (DateTimeParseException e) {
            return LocalDateTime.now();
        }
    }
}
