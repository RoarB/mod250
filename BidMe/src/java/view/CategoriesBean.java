/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import boundary.CategoryFacade;
import entities.Category;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Roar, Sanaullah
 */
@Named(value = "categoriesBean")
@RequestScoped
public class CategoriesBean {

    @EJB
    private CategoryFacade categoryFacade;
    /**
     * Creates a new instance of CategoriesBean
     */
    public CategoriesBean() {
    }
    
    public List<Category> getCategories() {
        return categoryFacade.findAll();
    }
    
    public String getCategoryName(Long categoryId) {
        return categoryFacade.find(categoryId).getCategoryName();
    }
}
