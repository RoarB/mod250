/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import entities.BidMeUser;
import boundary.UserFacade;
import entities.Address;
import entities.PhoneNumber;
import javax.inject.Named;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author roar
 */
@Named(value = "createUserBean")
@RequestScoped
public class CreateUserBean implements Serializable {
    String firstName;
    String lastName;
    String username;
    String password;
    String street;
    String city;
    String postcode;
    String country;
    String phonePrefix;
    String phoneNumber;
    
    BidMeUser user;
    
   @EJB
    private UserFacade userFacade;
    
    public CreateUserBean() {
        this.user = new BidMeUser();
    }
    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public String postUser() {
        this.user.setUsername(this.username);
        this.user.setPassword(this.password);
        this.user.setFirstName(this.firstName);
        this.user.setLastName(this.lastName);
        Address adr = new Address();
        adr.setCity(city);
        adr.setCountry(country);
        adr.setPostcode(postcode);
        adr.setStreet(street);
        this.user.setAddress(adr);
        PhoneNumber pho = new PhoneNumber();
        pho.setCountryCode(Integer.parseInt(phonePrefix));
        pho.setLocalNumber(Integer.parseInt(phoneNumber));
        this.userFacade.create(this.user);
        return "createUserResponse?faces-redirect=true";
    }   
    
    
}
