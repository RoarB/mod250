/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import boundary.LoggedInUserSessionBean;
import boundary.LoginException;
import entities.BidMeUser;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author roar
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {
    @EJB
    private LoggedInUserSessionBean loggedInUserSB;

    String response;

    public String getResponse() {
        return response;
    }
    
    public String getLoggedInUserName() {
        return loggedInUserSB.getLoggedInUsername();
    }
    
    public String getLoggedInFullname() {
        BidMeUser user = loggedInUserSB.getLoggedInUser();
        if (user != null)
            return user.getFirstName() + " " + user.getLastName();
        return "";
    }
    
    public BidMeUser getLoggedInUser() {
        return loggedInUserSB.getLoggedInUser();
    }
    
    public void logout() {
        response = "User logged out";
        loggedInUserSB.logout();
    }
    
    public String login(String username, String password) {
        try {
            loggedInUserSB.login(username, password);
        }
        catch (LoginException e) {
            response = e.getMessage();
            return "login";
        }
        response = "User " + username + " logged in";
        return "index?faces-redirect=true";
    }
    
    public boolean isLoggedIn() {
        return loggedInUserSB.getLoggedInUser() != null;
    }
    
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }
    
}
