/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import boundary.ProductFacade;
import entities.Product;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author roar
 */
@Named(value = "productsBean")
@RequestScoped
public class ProductsBean {

    private String textFilter;
    private List<Product> filteredList;
    @PersistenceContext
    private EntityManager em;
    
    @EJB
    private ProductFacade productFacade;
        
    public List<Product> getProducts(Integer numberOfProducts) {
        Query q = em.createQuery("SELECT p FROM Product p");
        return q.setMaxResults(numberOfProducts).getResultList();
    }
    
    public List<Product> getFilteredProductList(String textFilter) {
        return productFacade.getFilteredProductList(textFilter);
    }

    public List<Product> getFilteredList() {
        return filteredList;
    }

    public void setFilteredList(List<Product> filteredList) {
        this.filteredList = filteredList;
    }

    public String getTextFilter() {
        return textFilter;
    }

    public void setTextFilter(String textFilter) {
        this.textFilter = textFilter;
    }
    
    public String filterAuctions() {
        filteredList = productFacade.getFilteredProductList(textFilter);
        //return "auctionList?faces-redirect=true";
        return "auctionList";
    }
    
    /**
     * Creates a new instance of ProductsBean
     */
    public ProductsBean() {
    }
    
}
