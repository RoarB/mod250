/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import boundary.BidFacade;
import boundary.ProductFacade;
import boundary.UserFacade;
import entities.Bid;
import entities.BidMeUser;
import entities.Product;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author Sanaullah, Roar
 */
@Named(value = "bidBean")
@RequestScoped
public class BidBean {
    private String amount;
    
    private Bid bid;
    
    @Inject
    private LoginBean loginBean;
    @EJB
    private ProductFacade productFacade;
    @EJB
    private UserFacade userFacade;
    @EJB
    private BidFacade bidFacade;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Bid getBid() {
        return bid;
    }

    public void setBid(Bid bid) {
        this.bid = bid;
    }
    
    public String postBid(Long productId) {
        if (bid == null)
            bid = new Bid();
        Product p = productFacade.find(productId);
        BidMeUser user = loginBean.getLoggedInUser();
        bid.setProduct(p);
        bid.setBidder(user);
        bid.setAmount(Double.parseDouble(amount));
        bidFacade.create(bid);
        return "viewProduct?faces-redirect=true&productId=" + productId.toString();
    }
    
    /**
     * Creates a new instance of BidBean
     */
    public BidBean() {
    }
    
}
