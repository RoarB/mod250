/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 *
 * @author roar
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/NotifyBuyer"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class NotifyBuyerBean implements MessageListener {

    public NotifyBuyerBean() {
    }
    
    @Override
    public void onMessage(Message message) {
        TextMessage textMsg = (TextMessage) message;
        try {
            Logger.getLogger(NotifyBuyerBean.class.getName()).log(Level.INFO, null, textMsg.getText());
        } catch (JMSException ex) {
            Logger.getLogger(NotifyBuyerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
