/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soap;

import boundary.BidFacade;
import boundary.ProductFacade;
import boundary.UserFacade;
import entities.Bid;
import entities.BidMeUser;
import entities.Product;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Sanaullah, Roar
 */
@WebService(serviceName = "BidMeSOAP")
public class BidMeSOAP {

    @EJB
    private ProductFacade productFacade;
    
    @EJB
    private BidFacade bidFacade;
    
    @EJB
    private UserFacade userFacade;
    /**
     * Web service operation
     * @return 
     */
    @WebMethod(operationName = "getActiveAuctions")
    public List<Product> getActiveAuctions() {
        return productFacade.getActiveAuctions();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "bidForAuction")
    public BidResponseMessage bidForAuction(@WebParam(name = "productId") Long productId, 
            @WebParam(name = "username") String username, @WebParam(name = "amount") Double amount) {
        Product p = productFacade.find(productId);
        if (p == null)
            return new BidResponseMessage("Product not found", 440);
        
        Bid currentBid = productFacade.getCurrentBid(p.getId());
        if ((currentBid != null) && (amount <= currentBid.getAmount()))
            return new BidResponseMessage("Bid is not higher than current bid", 441);
        
        BidMeUser user = userFacade.find(username);
        if (user == null)
            return new BidResponseMessage("User not found", 442);
        
        Bid newBid = new Bid();
        newBid.setProduct(p);
        newBid.setBidder(user);
        newBid.setAmount(amount);
        
        bidFacade.create(newBid);
        return new BidResponseMessage("Bid placed", 200);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getCurrentBid")
    public Bid getCurrentBid(@WebParam(name = "productId") Long productId) {
        return productFacade.getCurrentBid(productId);
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "getUser")
    public BidMeUser getUser(@WebParam(name = "username") String username) {
        return userFacade.find(username);
        
    }
    
}
