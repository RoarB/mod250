/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soap;

import java.io.Serializable;

/**
 *
 * @author roar
 */
public class BidResponseMessage  implements Serializable {
    private String Message;
    private Integer StatusCode;

    public String getMessage() {
        return Message;
    }

    public Integer getStatusCode() {
        return StatusCode;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public void setStatusCode(Integer StatusCode) {
        this.StatusCode = StatusCode;
    }

    public BidResponseMessage(String Msg, Integer StatusCode) {
        this.Message = Msg;
        this.StatusCode = StatusCode;
    }

    @Override
    public String toString() {
        return StatusCode + ": " + Message; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
