/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author roar
 */
@Entity
public class BidMeGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String groupname;

    @ManyToMany
    @JoinTable(name="USER_GROUP", 
            joinColumns = @JoinColumn(name = "groupname", referencedColumnName = "groupname"), 
            inverseJoinColumns = @JoinColumn(name = "username", referencedColumnName = "username"))
    private List<BidMeUser> users;    
    
    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    @XmlTransient
    public List<BidMeUser> getUsers() {
        return users;
    }

    public void setUsers(List<BidMeUser> users) {
        this.users = users;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupname != null ? groupname.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BidMeGroup)) {
            return false;
        }
        BidMeGroup other = (BidMeGroup) object;
        if ((this.groupname == null && other.groupname != null) || (this.groupname != null && !this.groupname.equals(other.groupname))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.BidMeGroup[ groupname=" + groupname + " ]";
    }
    
}
