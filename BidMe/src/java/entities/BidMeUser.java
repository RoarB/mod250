/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author roar
 */
@Entity
@XmlRootElement
public class BidMeUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    @Embedded
    private Address address;
    @Embedded
    private PhoneNumber phoneNumber;
    @OneToMany(mappedBy = "seller")
    private List<Product> productsForSale;
    @OneToMany(mappedBy = "bidder")
    private List<Bid> bids;
    @ManyToMany(mappedBy = "users")
    private List<BidMeGroup> groups;

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @XmlTransient
    public List<Product> getProductsForSale() {
        return productsForSale;
    }

    @XmlTransient
    public List<Bid> getBids() {
        return bids;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlTransient
    public List<BidMeGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<BidMeGroup> groups) {
        this.groups = groups;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (username != null ? username.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BidMeUser)) {
            return false;
        }
        BidMeUser other = (BidMeUser) object;
        if ((this.username == null && other.username != null) || (this.username != null && !this.username.equals(other.username))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "bidme.User[ username=" + username + " ]";
    }

}
