/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author Roar, Sanaullah
 */
@Embeddable
public class Address implements Serializable {
    private String street;
    private String city;
    private String postcode;
    private String country;

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String Country) {
        this.country = Country;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (street != null ? street.hashCode() : 0);
        hash += (city != null ? city.hashCode() : 0);
        hash += (postcode != null ? postcode.hashCode() : 0);
        hash += (country != null ? country.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Address)) {
            return false;
        }
        Address other = (Address) object;
        if ((this.street == null && other.street != null) || (this.street != null && !this.street.equals(other.street))
          || (this.country == null && other.country != null) || (this.country != null && !this.country.equals(other.country))
          || (this.postcode == null && other.postcode != null) || (this.postcode != null && !this.postcode.equals(other.postcode))
          || (this.city == null && other.city != null) || (this.city != null && !this.city.equals(other.city))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return street + "," + postcode + "," + city + "," + country;
    }
    
}
