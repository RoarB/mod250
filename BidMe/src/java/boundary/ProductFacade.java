/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Bid;
import entities.Product;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Roar, Sanaullah
 */
@Stateless
//@RolesAllowed("Users_role")
public class ProductFacade extends AbstractFacade<Product> {

    @PersistenceContext(unitName = "BidMePU")
    private EntityManager em;
 
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductFacade() {
        super(Product.class);
    }

    public List<Product> getFilteredProductList(String textFilter) {
        TypedQuery<Product> q = em.createQuery("SELECT p FROM Product p WHERE LOWER(p.name) LIKE :textfilter", Product.class);
        return q.setParameter("textfilter", "%" + textFilter.toLowerCase() + "%").getResultList();
    }
    
    public Bid getCurrentBid(Long productId) {
        Product p = find(productId);
        return  p.getHighestBid();
    }
    
    public List<Product> getActiveAuctions() {
        TypedQuery<Product> q = em.createQuery("SELECT p FROM Product p WHERE (p.auctionStartDate < current_timestamp) and (p.auctionEndDate > current_timestamp)", Product.class);
        return q.getResultList();
    }
}
