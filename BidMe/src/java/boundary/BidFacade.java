/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Bid;
import entities.BidMeUser;
import entities.Product;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Roar, Sanaullah
 */
@Stateless
public class BidFacade extends AbstractFacade<Bid> {

    @PersistenceContext(unitName = "BidMePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @Override
    public void create(Bid bid) {
        Product product = bid.getProduct();
        BidMeUser bidder = bid.getBidder();

        // Update the bids of the product and the bidder
        product.getBids().add(bid);
        bidder.getBids().add(bid);
        
        // Persist the bid, merge product and bidder
        super.create(bid);
        em.merge(product);
        em.merge(bidder);
    }

    public BidFacade() {
        super(Bid.class);
    }
}
