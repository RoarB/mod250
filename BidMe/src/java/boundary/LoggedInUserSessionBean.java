/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.BidMeUser;
import javax.ejb.EJB;
import javax.ejb.Stateful;

/**
 *
 * @author Roar, Sanaullah
 */
@Stateful
public class LoggedInUserSessionBean {
    @EJB
    UserFacade userFacade;
    BidMeUser loggedInUser;
    
    public void login(String username, String password) throws LoginException {
       loggedInUser = userFacade.find(username);
       if (loggedInUser == null)
           throw new LoginException("User not found: " + username);
       if (!loggedInUser.getPassword().equals(password)) {
           loggedInUser = null;
           throw new LoginException("Incorrect password for user: " + username);
       }
    }
    
    public void logout() {
        loggedInUser = null;
    }

    public BidMeUser getLoggedInUser() {
        return loggedInUser;
    }
    
    public String getLoggedInUsername() {
        if (loggedInUser == null)
            return "";
        return loggedInUser.getUsername();
    }
}
