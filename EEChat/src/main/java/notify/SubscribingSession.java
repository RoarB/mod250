package notify;

import javax.websocket.Session;

/**
 * Class representing a web socket session and a channel subscription
 * @author Roar, Sanaullah
 */
public class SubscribingSession {
    Session session;
    String channelSubscription;

    /**
     * Construct a new SubscribingSession from the given session
     * @param session The session to associate this object with
     */
    public SubscribingSession(Session session) {
        this.session = session;
    }

    /**
     * Get the web socket session
     * @return The web socket session
     */
    public Session getSession() {
        return session;
    }

    /**
     * Get the channel subscription (if any) of this session
     * @return The name of the channel this object is subscribing to
     */
    public String getChannelSubscription() {
        return channelSubscription;
    }

    /**
     * Set the channel subscription of this session
     * @param channelName The new channel subscription
     */
    public void setChannelSubscription(String channelName) {
        this.channelSubscription = channelName;
    }
}
