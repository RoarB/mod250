package notify;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import rest.ChannelUpdatedListener;
import rest.ChannelUpdatedRelay;

/**
 * A class for notifying clients of updates of channels using web sockets
 * @author Roar, Sanaullah
 */
@ServerEndpoint("/notify")
public class NotifySocket implements ChannelUpdatedListener {
    final private Set<SubscribingSession> peers = Collections.synchronizedSet(new HashSet<SubscribingSession>());
    @EJB
    private ChannelUpdatedRelay channelUpdatedRelay;
    private Boolean listenerRegistered = false;
    
    /**
     * Event handler for messages from the web socket. It expects messages on the
     * form "subscribe:channelName", and will set up a subscription accordingly
     * @param message The message received from the web socket
     * @param session The web socket session
     * @return The response (always empty)
     */
    @OnMessage
    public String onMessage(String message, Session session) {
        String[] messageParts = message.split(":");
        if (!"subscribe".equals(messageParts[0]))
            return "";
        findSubscribingSession(session).setChannelSubscription(messageParts[1]);
        return "";
    }
    
    /**
     * The event handler for the web socket connected event
     * @param peer The new web socket session
     */
    @OnOpen
    public void onOpen (Session peer) {
        EnsureListenerRegistered();
        peers.add(new SubscribingSession(peer));
    }

    /**
     * The event handler for the web socket closed event
     * @param peer The web socket session
     */
    @OnClose
    public void onClose (Session peer) {
        SubscribingSession subSession = findSubscribingSession(peer);
        if (subSession != null)
            peers.remove(subSession);
    }

    /**
     * Event handler for the channel updated event. Will send a message to all
     * sessions that subscribe to this channel.
     * @param channelName The name of the updated channel.
     */
    @Override
    public void channelUpdated(String channelName) {
        for (SubscribingSession subSession: peers) {
            if (subSession.getChannelSubscription().equals(channelName)) {
                try {
                    subSession.getSession().getBasicRemote().sendText(channelName);
                } catch (IOException ex) {
                    Logger.getLogger(NotifySocket.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    private SubscribingSession findSubscribingSession(Session session) {
        for (SubscribingSession subSession: peers) {
            if (subSession.getSession().equals(session)) {
                return subSession;
            }
        }
        return null;
    }

    private void EnsureListenerRegistered() {
        if (!listenerRegistered)
            channelUpdatedRelay.RegisterListener(this);
        listenerRegistered = true;
    }
}
