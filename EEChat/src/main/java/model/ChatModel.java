package model;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Singleton;

/**
 * The model of the chat application, consisting of channels (with messages) and
 * users
 * @author Roar, Sanaullah
 */
@Singleton
public class ChatModel {
    private final Map<String, Channel> channels = new HashMap<>();
    private final Map<String, User> users = new HashMap<>();

    /**
     * Construct the chat model
     */
    public ChatModel() {
        // Put some default data into the model
        channels.put("Bergen", new Channel("Bergen"));
        channels.put("MOD250", new Channel("MOD250"));
        channels.put("JavaEE", new Channel("JavaEE"));
        users.put("roar", new User("roar", "roar", "Roar Bergheim"));
        users.put("sanaullah", new User("sanaullah", "sanaullah", "Sanaullah Afridi"));
    }

    /**
     * Get the map of channels
     * @return The map of channels
     */
    public Map<String, Channel> getChannels() {
        return channels;
    }

    /**
     * Get the map of users
     * @return The map of users
     */
    public Map<String, User> getUsers() {
        return users;
    }
}
