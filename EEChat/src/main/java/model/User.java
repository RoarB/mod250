package model;

/**
 * Class representing a user
 * @author Roar, Sanaullah
 */
public class User {
    private String username;
    private transient String password;
    private String fullname;

    /**
     * Construct a new user
     */
    public User() {
    }
    
    /**
     * Construct a new user with the given username, password and full name
     * @param username The username of this user
     * @param password The password of this user
     * @param fullname The full name of this user
     */
    public User(String username, String password, String fullname) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
    }
    
    /**
     * Get the username of this user
     * @return The username of this user
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the username of this user
     * @param username The new username of this user
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the password of this user
     * @return The password of this user
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the password of this user
     * @param password The new password of this user
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get the full name of this user
     * @return The full name of this user
     */
    public String getFullname() {
        return fullname;
    }

    /**
     * Set the full name of this user
     * @param fullname The new full name of this user
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
}
