package model;

import java.util.HashSet;
import java.util.Set;

/**
 * Class representing a message in a channel
 * @author Roar, Sanaullah
 */
public class Message {
    private User author;
    private String text;
    private final Set<User> likedBy = new HashSet<>();
    private Long id;
    
    private static Long idCounter = 0L;

    /**
     * Construct a new message
     */
    public Message() {
        id = idCounter++;
    }

    /**
     * Construct a new message with the given author (User) and text.
     * @param author The author of this message
     * @param text The text of this message
     */
    public Message(User author, String text) {
        this();
        this.author = author;
        this.text = text;
    }

    /**
     * Get the author of this message
     * @return The author of the message
     */
    public User getAuthor() {
        return author;
    }

    /**
     * Set the author of this message
     * @param author The new author
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * Get the text of this message
     * @return The text of this message
     */
    public String getText() {
        return text;
    }

    /**
     * Set the text of this message
     * @param text The new text of this message
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Get the set of users that have liked this message
     * @return The set of users that like this message
     */
    public Set<User> getLikedBy() {
        return likedBy;
    }

    /**
     * Get the ID of this message
     * @return The ID of this message
     */
    public Long getId() {
        return id;
    }
}
