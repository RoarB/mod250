package model;

import java.util.HashMap;
import java.util.Map;

/**
 * Class representing a channel in the chat room
 * @author Roar, Sanaullah
 */
public class Channel {
    private String channelName;
    private final Map<Long, Message> messages = new HashMap<>();

    /**
     * Construct a channel
     */
    public Channel() {
    }

    /**
     * Construct a channel with a name
     * @param channelName The name of the channel
     */
    public Channel(String channelName) {
        this.channelName = channelName;
    }

    /**
     * Get the channel name. This is also used as the ID of the channel.
     * @return The channel name
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * Set the channel name
     * @param channelName The new name of the channel
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    /**
     * Get the map of messages. The key of the map is the message ID.
     * @return The map of messages.
     */
    public Map<Long, Message> getMessages() {
        return messages;
    }
}
