package rest;

/**
 * Interface for listeners of the channelUpdated event
 * @author Roar, Sanaullah
 */
public interface ChannelUpdatedListener {

    /**
     * Channel updated event
     * @param channelName The name of the channel that was updated
     */
    void channelUpdated(String channelName);
}
