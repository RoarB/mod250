package rest;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.ext.*;
import com.google.gson.*;

/**
 * A GSON Message body reader and writer for the Object class. This converts any 
 * Object to and from JSON.
 * @author Roar, Sanaullah
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public final class MessageBodyHandlerObject implements MessageBodyWriter<Object>,
    MessageBodyReader<Object> {
 
    private static final String UTF_8 = "UTF-8";
 
    private final Gson gson;

    public MessageBodyHandlerObject() {
        gson = new Gson();
    }
    
    @Override
    public boolean isReadable(Class<?> type, Type genericType,
        java.lang.annotation.Annotation[] annotations, MediaType mediaType) {
        return true;
    }
 
    @Override
    public Object readFrom(Class<Object> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws UnsupportedEncodingException, IOException {
        try (InputStreamReader streamReader = new InputStreamReader(entityStream, UTF_8)) {
            Type jsonType;
            if (type.equals(genericType)) {
                jsonType = type;
            } else {
                jsonType = genericType;
            }
            return gson.fromJson(streamReader, jsonType);
        }
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(Object object, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(Object object, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        try (OutputStreamWriter writer = new OutputStreamWriter(entityStream, UTF_8)) {
            Type jsonType;
            if (type.equals(genericType)) {
                jsonType = type;
            } else {
                jsonType = genericType;
            }
            gson.toJson(object, jsonType, writer);
        }
    }
}
