package rest;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import model.Channel;
import model.ChatModel;
import model.Message;
import model.User;

/**
 * REST interface for liking messages
 * @author Roar, Sanaullah
 */
@Path("/channels/{channelName}/messages/{msgId}/likedBy")
public class Likes {
    @EJB
    private ChatModel chatModel;
    @EJB
    private UserAuthentication auth;
    @EJB
    private ChannelUpdatedRelay channelUpdatedRelay;
    @Context
    private HttpHeaders headers;

    /**
     * Add a like from the authenticated user to a message
     * @param channelName The name of the channel that contains the message
     * @param msgId The ID of the message
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addLike(@PathParam("channelName") String channelName, @PathParam("msgId") Long msgId) {
        User user = auth.ensureAuthenticatedUser(headers);
        Channel chan = chatModel.getChannels().get(channelName);
        Message msg = chan.getMessages().get(msgId);
        msg.getLikedBy().add(user);
        channelUpdatedRelay.OnChannelUpdated(channelName);
    }
}
