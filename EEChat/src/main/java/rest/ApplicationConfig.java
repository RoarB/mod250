package rest;

import java.util.Set;
import javax.ws.rs.core.Application;

@javax.ws.rs.ApplicationPath("/api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(rest.Channels.class);
        resources.add(rest.Likes.class);
        resources.add(rest.MessageBodyHandlerMessage.class);
        resources.add(rest.MessageBodyHandlerObject.class);
        resources.add(rest.Messages.class);
        resources.add(rest.Users.class);
    }
}
