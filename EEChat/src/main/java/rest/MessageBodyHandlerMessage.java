package rest;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.ext.*;
import com.google.gson.*;
import java.util.*;
import javax.ejb.EJB;
import model.ChatModel;
import model.Message;
import model.User;

/**
 * A GSON Message body reader and writer for the Message class. This converts a 
 * Message to and from JSON.
 * @author Roar, Sanaullah
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public final class MessageBodyHandlerMessage implements MessageBodyWriter<Message>,
    MessageBodyReader<Message> {
 
    @EJB
    private ChatModel chatModel;
    private static final String UTF_8 = "UTF-8";
    private final Gson gson;

    /**
     * Construct a new instance
     */
    public MessageBodyHandlerMessage() {
        gson = new GsonBuilder().registerTypeAdapter(Message.class, new MessageDeserializer()).create();
    }
    
    /**
     * Check if the given message is readable. Always true.
     * @param type The specific type of the object
     * @param genericType The generic type of the object
     * @param annotations The annotations of the object
     * @param mediaType The media type of the object
     * @return Always true
     */
    @Override
    public boolean isReadable(Class<?> type, Type genericType,
        java.lang.annotation.Annotation[] annotations, MediaType mediaType) {
        return true;
    }
 
    /**
     * Convert a JSON string to a Message object
     * @param type The specific type of the object
     * @param genericType The generic type of the object
     * @param annotations The annotations of the object
     * @param mediaType The media type of the object
     * @param httpHeaders The HTTP headers of the request
     * @param entityStream The stream to read from
     * @return The Message object
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    @Override
    public Message readFrom(Class<Message> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws UnsupportedEncodingException, IOException {
        try (InputStreamReader streamReader = new InputStreamReader(entityStream, UTF_8)) {
            Type jsonType;
            if (type.equals(genericType)) {
                jsonType = type;
            } else {
                jsonType = genericType;
            }
            return gson.fromJson(streamReader, jsonType);
        }
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(Message object, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(Message object, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        try (OutputStreamWriter writer = new OutputStreamWriter(entityStream, UTF_8)) {
            Type jsonType;
            if (type.equals(genericType)) {
                jsonType = type;
            } else {
                jsonType = genericType;
            }
            gson.toJson(object, jsonType, writer);
        }
    }
    
    /**
     * Custom deserializer for JSON messages. The reason for it is to look up
     * the user. The JSON just contains a username, but we need a User object.
     */
    private class MessageDeserializer implements JsonDeserializer<Message> {
        @Override
        public Message deserialize(JsonElement json, Type type, JsonDeserializationContext jdc) throws JsonParseException {
            Message msg = new Message();
            Set<Map.Entry<String, JsonElement>> messageProperties = json.getAsJsonObject().entrySet();
            for (Iterator<Map.Entry<String, JsonElement>> i = messageProperties.iterator(); i.hasNext();) {
                Map.Entry<String, JsonElement> entry = i.next();
                switch (entry.getKey()) {
                    case "text":
                        msg.setText(entry.getValue().getAsString());
                        break;
                    case "author":
                        User user = chatModel.getUsers().get(entry.getValue().getAsString());
                        msg.setAuthor(user);
                }
            }
            return msg;
        }
    }
}
