package rest;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import model.ChatModel;
import model.User;

/**
 * REST interface for the users
 * @author Roar, Sanaullah
 */
@Path("/users")
public class Users {
    @EJB
    private ChatModel chatModel;
    @EJB
    private UserAuthentication auth;
    @Context
    HttpHeaders headers;
    
    /**
     * Get the user details of a user
     * @param username The username of the user
     * @return The user if found
     * @throws NotAuthorizedException
     */
    @GET
    @Path("{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser(@PathParam("username") String username) {
        User reqUser = chatModel.getUsers().get(username);
        User authUser = auth.getAuthenticatedUser(headers);
        if (reqUser != authUser)
            throw new NotAuthorizedException("Authorization failed");
        return authUser;
    }
    
    /**
     * Add a new user
     * @param newUser The new user
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addUser(User newUser) {
        auth.ensureAuthenticatedUser(headers);
        chatModel.getUsers().put(newUser.getUsername(), newUser);
    }
}
