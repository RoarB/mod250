package rest;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import model.Channel;
import model.ChatModel;

/**
 * REST interface for the channels
 * @author Roar, Sanaullah
 */
@Path("/channels")
public class Channels {
    @Context
    private HttpHeaders headers;
    @EJB
    private ChatModel chatModel;
    @EJB
    private UserAuthentication auth;
    
    /**
     * Construct a new instance
     */
    public Channels() {
    }

    /**
     * Get all the channels 
     * @return A map of all the channels (returned as Object to get the
     * JSON serializer to kick in)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Object getChannels() {
        return chatModel.getChannels();
    }
    
    /**
     * Add a new channel
     * @param newChannel The new channel
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addChannel(Channel newChannel) {
        auth.ensureAuthenticatedUser(headers);
        chatModel.getChannels().put(newChannel.getChannelName(), newChannel);
    }
}
