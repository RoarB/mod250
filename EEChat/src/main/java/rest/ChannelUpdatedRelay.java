package rest;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;

/**
 * Class that relays channel updated events from the REST service to
 * the web sockets.
 * @author Roar, Sanaullah
 */
@Singleton
public class ChannelUpdatedRelay {
    private final List<ChannelUpdatedListener> listeners = new ArrayList<>();
    
    /**
     * Register a new listener for the channel updated event
     * @param listener The new listener
     */
    public void RegisterListener(ChannelUpdatedListener listener) {
        listeners.add(listener);
    }
    
    /**
     * Triggers the channel updated event for all listeners
     * @param channelName The name of the channel
     */
    public void OnChannelUpdated(String channelName) {
        for (ChannelUpdatedListener listener : listeners) {
            listener.channelUpdated(channelName);
        }
    }
}
