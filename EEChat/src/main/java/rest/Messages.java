package rest;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import model.Channel;
import model.ChatModel;
import model.Message;

/**
 * REST interface for the messages
 * @author roar
 */
@Path("/channels/{channelName}/messages")
public class Messages {
    @EJB
    private ChatModel chatModel;
    @EJB
    private UserAuthentication auth;
    @EJB
    private ChannelUpdatedRelay channelUpdatedRelay;
    @Context
    private HttpHeaders headers;

    /**
     * Get all the messages of a channel
     * @param channelName The name of the channel
     * @return A map of all the messages in this channel
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Object getMessages(@PathParam("channelName") String channelName) {
        auth.ensureAuthenticatedUser(headers);
        Channel chan = chatModel.getChannels().get(channelName);
        return chan.getMessages();
    }
    
    /**
     * Adds a new message to this channel
     * @param channelName The name of the channel
     * @param newMessage The new message to add to the channel
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addMessage(@PathParam("channelName") String channelName, Message newMessage) {
        auth.ensureAuthenticatedUser(headers);
        Channel chan = chatModel.getChannels().get(channelName);
        chan.getMessages().put(newMessage.getId(), newMessage);
        channelUpdatedRelay.OnChannelUpdated(channelName);
    }
}
