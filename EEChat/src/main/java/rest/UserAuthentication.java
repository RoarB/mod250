package rest;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.HttpHeaders;
import model.ChatModel;
import model.User;

/**
 * Singleton EJB for authenticating users
 * @author Roar, Sanaullah
 */
@Singleton
public class UserAuthentication {
    @EJB
    private ChatModel chatModel;
    
    /**
     * Tries to authenticate a user from the HTTP headers. Only supports Basic
     * authentication. Throws exception if authentication fails.
     * @param headers The HTTP headers with the "Authentication" header
     * @return The user if authentication was successful
     * @throws NotAuthorizedException
     */
    public User ensureAuthenticatedUser(HttpHeaders headers) {
        User user = getAuthenticatedUser(headers);
        if (user == null)
            throw new NotAuthorizedException("Authorization failed");
        return user;
    }
    
    /**
     * Get the authenticated user from the HTTP headers. Only supports Basic
     * authentication.
     * @param headers The HTTP headers with the "Authentication" header
     * @return The authenticated user if it was successful, null otherwise.
     */
    public User getAuthenticatedUser(HttpHeaders headers) {
        String authHeader = headers.getHeaderString("Authorization");
        if (authHeader == null)
            return null;
        String[] authSplit = authHeader.split(" ");
        if (!"Basic".equals(authSplit[0]))
            return null;
        String decodedAuth = new String(Base64.getDecoder().decode(authSplit[1]), StandardCharsets.UTF_8);
        String[] decodedSplit = decodedAuth.split(":");
        User user = chatModel.getUsers().get(decodedSplit[0]);
        if (user == null)
            return null;
        if (user.getPassword() == null ? decodedSplit[1] != null : !user.getPassword().equals(decodedSplit[1]))
            return null;
        return user;
    }
}
